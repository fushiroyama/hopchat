# HopChat

A tiny chat app to handle `@mention`, `(emoticon)`, and `http://links`.

![Message Input](./images/overview01.png)
![Message Processing](./images/overview02.png)
![Message List](./images/overview03.png)

## Message Parser

`us.shiroyama.android.hopchat.util.parser.MessageParser#parse` takes String as Input, and returns `us.shiroyama.android.hopchat.entity.Message` as Output.

See `MessageParserTest` for quick overview.

`Message` object can be serialized into JSON using `Gson` like below.

```json
{
  "mentions": [
    "bob",
    "john"
  ],
  "emoticons": [
    "megusta",
    "coffee"
  ],
  "links": [
    {
      "url": "https://twitter.com/jdorfman/status/430511497475670016",
      "title": "Twitter / jdorfman: nice @littlebigdetail from ..."
    }
  ]
}
```

See `MessageTest` for quick overview.

## Link Title Fetching

It may be useful if `MessageParser` automatically fetches each link's title, hower it abviously slows down the parse function to return dramatically.

So I decided to move that feature in `us.shiroyama.android.hopchat.viewmodel.LinkViewModel`.

Thus message parsing returns very quickly and link's title is loaded asynchronously.


## Architecture

This app is made using DataBinding.

## License

```
Copyright 2017 Fumihiko Shiroyama (fu.shiroyama@gmail.com)
Licensed under the Apache License, Version 2.0 (the "License")
```
