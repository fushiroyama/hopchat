package us.shiroyama.android.hopchat.view.activity;

import android.os.Bundle;

import us.shiroyama.android.hopchat.R;
import us.shiroyama.android.hopchat.view.fragment.ChatFragment;

/**
 * Chat {@link DaggerActivity}
 *
 * @author Fumihiko Shiroyama (fu.shiroyama@gmail.com)
 */
public class ChatActivity extends DaggerActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        getComponent().inject(this);

        replaceFragment(ChatFragment.newInstance(), R.id.container);
    }
}
