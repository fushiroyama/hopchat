package us.shiroyama.android.hopchat.di.modules;

import android.app.Application;
import android.support.annotation.NonNull;

import dagger.Module;
import dagger.Provides;

/**
 * Application {@link Module}
 *
 * @author Fumihiko Shiroyama (fu.shiroyama@gmail.com)
 */

@Module(includes = {
        MessageModule.class,
        GsonModule.class,
        OkHttpModule.class
})
public class ApplicationModule {
    @NonNull
    private final Application application;

    public ApplicationModule(@NonNull Application application) {
        this.application = application;
    }

    @Provides
    public Application providesApplication() {
        return application;
    }
}
