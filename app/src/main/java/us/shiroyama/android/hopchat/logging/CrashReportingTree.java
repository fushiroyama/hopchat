package us.shiroyama.android.hopchat.logging;

import android.util.Log;

import com.crashlytics.android.Crashlytics;

import timber.log.Timber.Tree;

/**
 * Crash Reporting {@link Tree}
 *
 * @author Fumihiko Shiroyama (fu.shiroyama@gmail.com)
 */

public class CrashReportingTree extends Tree {
    @Override
    protected void log(int priority, String tag, String message, Throwable t) {
        if (priority == Log.VERBOSE || priority == Log.DEBUG || priority == Log.INFO) {
            return;
        }

        if (t != null) {
            Crashlytics.logException(t);
        }
    }
}
