package us.shiroyama.android.hopchat.di.components;

import dagger.Subcomponent;
import us.shiroyama.android.hopchat.di.modules.ActivityModule;
import us.shiroyama.android.hopchat.di.modules.FragmentModule;
import us.shiroyama.android.hopchat.di.scopes.ActivityScope;
import us.shiroyama.android.hopchat.view.activity.ChatActivity;

/**
 * Activity {@link Subcomponent}
 *
 * @author Fumihiko Shiroyama (fu.shiroyama@gmail.com)
 */

@ActivityScope
@Subcomponent(modules = ActivityModule.class)
public interface ActivityComponent {
    void inject(ChatActivity activity);

    FragmentComponent plus(FragmentModule module);
}
