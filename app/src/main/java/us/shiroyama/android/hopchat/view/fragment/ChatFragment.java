package us.shiroyama.android.hopchat.view.fragment;


import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableList;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import us.shiroyama.android.hopchat.R;
import us.shiroyama.android.hopchat.databinding.FragmentChatBinding;
import us.shiroyama.android.hopchat.databinding.LinkBinding;
import us.shiroyama.android.hopchat.databinding.MessageCellBinding;
import us.shiroyama.android.hopchat.util.parser.MessageParser;
import us.shiroyama.android.hopchat.view.customview.BindingHolder;
import us.shiroyama.android.hopchat.view.customview.ListAdapter;
import us.shiroyama.android.hopchat.viewmodel.ChatViewModel;
import us.shiroyama.android.hopchat.viewmodel.LinkViewModel;
import us.shiroyama.android.hopchat.viewmodel.MessageViewModel;

/**
 * Chat {@link DaggerFragment}
 *
 * @author Fumihiko Shiroyama (fu.shiroyama@gmail.com)
 */

public class ChatFragment extends DaggerFragment {
    @Inject
    MessageParser messageParser;

    @Inject
    ChatViewModel viewModel;

    private FragmentChatBinding binding;

    public ChatFragment() {
    }

    public static ChatFragment newInstance() {
        return new ChatFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        getComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentChatBinding.inflate(inflater, container, false);
        binding.setViewModel(viewModel);

        Adapter adapter = new Adapter(getContext(), viewModel.messageViewModels);
        binding.recyclerView.setAdapter(adapter);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        return binding.getRoot();
    }

    private static class Adapter extends ListAdapter<MessageViewModel, BindingHolder<MessageCellBinding>> {
        Adapter(@NonNull Context context, @NonNull ObservableList<MessageViewModel> list) {
            super(context, list);

            list.addOnListChangedCallback(new ObservableList.OnListChangedCallback<ObservableList<MessageViewModel>>() {
                @Override
                public void onChanged(ObservableList<MessageViewModel> messageViewModels) {
                    notifyDataSetChanged();
                }

                @Override
                public void onItemRangeChanged(ObservableList<MessageViewModel> messageViewModels, int i, int i1) {
                    notifyItemRangeChanged(i, i1);
                }

                @Override
                public void onItemRangeInserted(ObservableList<MessageViewModel> messageViewModels, int i, int i1) {
                    notifyItemRangeInserted(i, i1);
                }

                @Override
                public void onItemRangeMoved(ObservableList<MessageViewModel> messageViewModels, int i, int i1, int i2) {
                    notifyItemMoved(i, i1);
                }

                @Override
                public void onItemRangeRemoved(ObservableList<MessageViewModel> messageViewModels, int i, int i1) {
                    notifyItemRangeRemoved(i, i1);
                }
            });
        }

        @Override
        public BindingHolder<MessageCellBinding> onCreateViewHolder(ViewGroup parent, int viewType) {
            return new BindingHolder<>(getContext(), parent, R.layout.message_cell);
        }

        @Override
        public void onBindViewHolder(BindingHolder<MessageCellBinding> holder, int position) {
            MessageViewModel viewModel = getItem(position);
            MessageCellBinding binding = holder.binding;

            for (LinkViewModel linkViewModel : viewModel.linkViewModels) {
                View linkView = LayoutInflater.from(getContext()).inflate(R.layout.link, null);
                binding.linkContainer.addView(linkView);
                LinkBinding linkBinding = DataBindingUtil.bind(linkView);
                linkBinding.setViewModel(linkViewModel);
            }

            binding.setViewModel(viewModel);
            binding.executePendingBindings();
        }
    }

}
