package us.shiroyama.android.hopchat.util.parser.exception;

/**
 * @author Fumihiko Shiroyama (fu.shiroyama@gmail.com)
 */

public class Fallback extends RuntimeException {
    public Fallback() {
    }

    public Fallback(String message) {
        super(message);
    }

    public Fallback(String message, Throwable cause) {
        super(message, cause);
    }

    public Fallback(Throwable cause) {
        super(cause);
    }
}
