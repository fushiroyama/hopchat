package us.shiroyama.android.hopchat.di.modules;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import dagger.Module;
import dagger.Provides;

/**
 * Fragment {@link Module}
 *
 * @author Fumihiko Shiroyama (fu.shiroyama@gmail.com)
 */

@Module
public class FragmentModule {
    @NonNull
    private final Fragment fragment;

    public FragmentModule(@NonNull Fragment fragment) {
        this.fragment = fragment;
    }

    @Provides
    public FragmentManager providesFragmentManager() {
        return fragment.getFragmentManager();
    }

    @Provides
    public Context providesContext() {
        return fragment.getContext();
    }
}
