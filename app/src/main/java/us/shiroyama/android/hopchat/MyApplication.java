package us.shiroyama.android.hopchat;

import android.app.Application;
import android.support.annotation.NonNull;

import com.crashlytics.android.Crashlytics;
import com.facebook.stetho.Stetho;
import com.facebook.stetho.timber.StethoTree;
import com.squareup.leakcanary.LeakCanary;

import io.fabric.sdk.android.Fabric;
import timber.log.Timber;
import us.shiroyama.android.hopchat.di.components.ApplicationComponent;
import us.shiroyama.android.hopchat.di.components.DaggerApplicationComponent;
import us.shiroyama.android.hopchat.di.modules.ApplicationModule;
import us.shiroyama.android.hopchat.logging.CrashReportingTree;

/**
 * Custom {@link Application}
 *
 * @author Fumihiko Shiroyama (fu.shiroyama@gmail.com)
 */

public class MyApplication extends Application {
    ApplicationComponent applicationComponent;

    @NonNull
    public ApplicationComponent getComponent() {
        return applicationComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();

        initLeakCanary();
        initFabric();
        initTimber();
        initStetho();
    }

    private void initLeakCanary() {
        if (LeakCanary.isInAnalyzerProcess(this)) {
            return;
        }
        LeakCanary.install(this);
    }

    private void initFabric() {
        Fabric.with(this, new Crashlytics());
    }

    private void initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
            Timber.plant(new StethoTree());
        } else {
            Timber.plant(new CrashReportingTree());
        }
    }

    private void initStetho() {
        Stetho.initializeWithDefaults(this);
    }

}
