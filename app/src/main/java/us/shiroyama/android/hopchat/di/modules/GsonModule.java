package us.shiroyama.android.hopchat.di.modules;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import us.shiroyama.android.hopchat.entity.Message;
import us.shiroyama.android.hopchat.entity.gson.EmoticonTypeAdapter;
import us.shiroyama.android.hopchat.entity.gson.MentionTypeAdapter;

/**
 * {@link Gson} {@link Module}
 *
 * @author Fumihiko Shiroyama (fu.shiroyama@gmail.com)
 */

@Module
public class GsonModule {
    @Singleton
    @Provides
    public GsonBuilder providesGsonBuilder() {
        return new GsonBuilder();
    }

    @Singleton
    @Provides
    public Gson providesGson(GsonBuilder gsonBuilder) {
        return gsonBuilder
                .excludeFieldsWithoutExposeAnnotation()
                .registerTypeAdapter(Message.Mention.class, MentionTypeAdapter.newInstance())
                .registerTypeAdapter(Message.Emoticon.class, EmoticonTypeAdapter.newInstance())
                .create();
    }
}
