package us.shiroyama.android.hopchat.viewmodel;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableField;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.IOException;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import timber.log.Timber;
import us.shiroyama.android.hopchat.BuildConfig;
import us.shiroyama.android.hopchat.entity.Message;
import us.shiroyama.android.hopchat.util.parser.MessageParser;

/**
 * ViewModel for Chat
 *
 * @author Fumihiko Shiroyama (fu.shiroyama@gmail.com)
 */

public class ChatViewModel extends BaseObservable {
    public final ObservableField<String> editTextMessage = new ObservableField<>();

    public final ObservableArrayList<MessageViewModel> messageViewModels = new ObservableArrayList<>();

    private final Context context;

    private final MessageParser messageParser;

    private final Gson gson;

    private final OkHttpClient okHttpClient;

    @Inject
    public ChatViewModel(Context context, MessageParser messageParser, Gson gson, OkHttpClient okHttpClient) {
        this.context = context;
        this.messageParser = messageParser;
        this.gson = gson;
        this.okHttpClient = okHttpClient;
    }

    public void onClickPost(View view) {
        String originalMessage = editTextMessage.get();
        Timber.d("originalMessage: %s", originalMessage);
        editTextMessage.set("");
        try {
            Message message = messageParser.parse(originalMessage);
            message.setMessage(originalMessage);
            String messageJson = gson.toJson(message);
            Timber.d("messageJson: %s", messageJson);

            if (BuildConfig.DEBUG) {
                Toast.makeText(context, messageJson, Toast.LENGTH_LONG).show();
            }

            MessageViewModel messageViewModel = new MessageViewModel(message, okHttpClient);
            messageViewModels.add(messageViewModel);
        } catch (IOException e) {
            Timber.d(e);
        }
    }
}
