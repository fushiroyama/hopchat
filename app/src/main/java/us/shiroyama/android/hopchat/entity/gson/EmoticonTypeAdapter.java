package us.shiroyama.android.hopchat.entity.gson;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

import us.shiroyama.android.hopchat.entity.Message.Emoticon;

/**
 * TypeAdapter for {@link Emoticon}
 *
 * @author Fumihiko Shiroyama (fu.shiroyama@gmail.com)
 */

public class EmoticonTypeAdapter implements JsonSerializer<Emoticon>, JsonDeserializer<Emoticon> {
    public static EmoticonTypeAdapter newInstance() {
        return new EmoticonTypeAdapter();
    }

    @Override
    public Emoticon deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        return new Emoticon(json.getAsString());
    }

    @Override
    public JsonElement serialize(Emoticon src, Type typeOfSrc, JsonSerializationContext context) {
        return new JsonPrimitive(src.getLabel());
    }
}
