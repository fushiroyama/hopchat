package us.shiroyama.android.hopchat.di.scopes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Activity {@link Scope}
 *
 * @author Fumihiko Shiroyama (fu.shiroyama@gmail.com)
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface ActivityScope {
}
