package us.shiroyama.android.hopchat.util.parser.exception;

/**
 * @author Fumihiko Shiroyama (fu.shiroyama@gmail.com)
 */

public class ExpectationFailure extends Exception {
    public ExpectationFailure() {
    }

    public ExpectationFailure(String message) {
        super(message);
    }

    public ExpectationFailure(String message, Throwable cause) {
        super(message, cause);
    }

    public ExpectationFailure(Throwable cause) {
        super(cause);
    }
}
