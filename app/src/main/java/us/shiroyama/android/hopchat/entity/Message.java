package us.shiroyama.android.hopchat.entity;

import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Message Entity
 *
 * @author Fumihiko Shiroyama (fu.shiroyama@gmail.com)
 */

public class Message {
    @Expose
    private List<Mention> mentions;

    @Expose
    private List<Emoticon> emoticons;

    @Expose
    private List<Link> links;

    /**
     * original message
     */
    private String message;

    public Message() {
    }

    public Message(String message) {
        this.message = message;
    }

    public List<Mention> getMentions() {
        return mentions;
    }

    public void setMentions(List<Mention> mentions) {
        this.mentions = mentions;
    }

    public List<Emoticon> getEmoticons() {
        return emoticons;
    }

    public void setEmoticons(List<Emoticon> emoticons) {
        this.emoticons = emoticons;
    }

    public List<Link> getLinks() {
        return links;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Mention Entity
     */
    public static class Mention {
        @Expose
        private String name;

        public Mention() {
        }

        public Mention(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    /**
     * Emoticon Entity
     */
    public static class Emoticon {
        @Expose
        private String label;

        public Emoticon() {
        }

        public Emoticon(String label) {
            this.label = label;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }
    }

    /**
     * Link Entity
     */
    public static class Link {
        @Expose
        private String url;

        @Expose
        private String title;

        public Link() {
        }

        public Link(String url) {
            this.url = url;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }

}
