package us.shiroyama.android.hopchat.util.parser;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

import timber.log.Timber;
import us.shiroyama.android.hopchat.entity.Message;
import us.shiroyama.android.hopchat.util.MessageUtils;
import us.shiroyama.android.hopchat.util.parser.exception.ExpectationFailure;
import us.shiroyama.android.hopchat.util.parser.exception.Fallback;

/**
 * Message Parser
 *
 * @author Fumihiko Shiroyama (fu.shiroyama@gmail.com)
 */

@Singleton
public class MessageParser {
    private char getNextChar(BufferedReader bufferedReader) throws IOException {
        return getNextChar(bufferedReader, true);
    }

    private char getNextChar(BufferedReader bufferedReader, boolean mark) throws IOException {
        if (mark) {
            bufferedReader.mark(1);
        }
        int i = bufferedReader.read();
        if (i == -1) {
            throw new EOFException();
        }
        return (char) i;
    }

    private void expectNextChar(BufferedReader bufferedReader, char expect) throws IOException, ExpectationFailure {
        expectNextChar(bufferedReader, expect, null);
    }

    private void expectNextChar(BufferedReader bufferedReader, char expect, Character alternate) throws IOException, ExpectationFailure {
        char c = getNextChar(bufferedReader);
        if (c != expect) {
            if (alternate != null && c == alternate) {
                bufferedReader.reset();
                throw new Fallback();
            }
            throw new ExpectationFailure();
        }
    }

    public Message parse(String source) throws IOException {
        Reader reader = new StringReader(source);
        BufferedReader bufferedReader = new BufferedReader(reader);

        Message message = new Message();
        List<Message.Mention> mentions = new ArrayList<>();
        List<Message.Emoticon> emoticons = new ArrayList<>();
        List<Message.Link> links = new ArrayList<>();
        message.setMentions(mentions);
        message.setEmoticons(emoticons);
        message.setLinks(links);

        try {
            while (true) {
                char c = getNextChar(bufferedReader);
                switch (c) {
                    case '@':
                        String mention = getString(bufferedReader, MessageUtils::isAlphaNumeric);
                        Timber.d("mention: %s", mention);
                        mentions.add(new Message.Mention(mention));
                        continue;
                    case '(':
                        String emoticon = getString(bufferedReader, c1 -> c1 != ')' && MessageUtils.isAlphaNumeric(c1));
                        if (MessageUtils.isValidEmoticon(emoticon)) {
                            Timber.d("emoticon: %s", emoticon);
                            emoticons.add(new Message.Emoticon(emoticon));
                        }
                        continue;
                    case 'h':
                        StringBuilder linkBuilder = new StringBuilder();
                        linkBuilder.append('h');

                        try {
                            expectNextChar(bufferedReader, 't');
                            linkBuilder.append('t');

                            expectNextChar(bufferedReader, 't');
                            linkBuilder.append('t');

                            expectNextChar(bufferedReader, 'p');
                            linkBuilder.append('p');

                            try {
                                expectNextChar(bufferedReader, 's', ':');
                                linkBuilder.append('s');
                            } catch (Fallback e) {
                                Timber.d("Alternate char found. Fallback.");
                            }

                            expectNextChar(bufferedReader, ':');
                            linkBuilder.append(':');

                            expectNextChar(bufferedReader, '/');
                            linkBuilder.append('/');

                            expectNextChar(bufferedReader, '/');
                            linkBuilder.append('/');
                        } catch (ExpectationFailure e) {
                            Timber.d("Expected char not found. continue.");
                            continue;
                        }

                        String forTheRest = getString(bufferedReader, (c1) -> !MessageUtils.isSeparator(c1));
                        linkBuilder.append(forTheRest);

                        String link = linkBuilder.toString();
                        Timber.d("link: %s", link);
                        links.add(new Message.Link(link));
                }
            }
        } catch (EOFException e) {
            Timber.d("EOF");
            return message;
        }
    }

    private String getString(BufferedReader bufferedReader, Predicator predicator) throws IOException {
        StringBuilder sb = new StringBuilder();
        try {
            char c = getNextChar(bufferedReader);
            while (predicator.predicate(c)) {
                sb.append(c);
                c = getNextChar(bufferedReader);
            }
            bufferedReader.reset();
        } catch (EOFException e) {
            Timber.d("EOF");
        }
        return sb.toString();
    }

    private interface Predicator {
        boolean predicate(char c);
    }

}
