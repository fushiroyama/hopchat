package us.shiroyama.android.hopchat.view.fragment;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;

import us.shiroyama.android.hopchat.di.components.FragmentComponent;
import us.shiroyama.android.hopchat.di.modules.FragmentModule;
import us.shiroyama.android.hopchat.view.activity.DaggerActivity;

/**
 * Dagger capable {@link Fragment}
 *
 * @author Fumihiko Shiroyama (fu.shiroyama@gmail.com)
 */

public class DaggerFragment extends Fragment {
    private FragmentComponent fragmentComponent;

    @NonNull
    public FragmentComponent getComponent() {
        if (fragmentComponent != null) {
            return fragmentComponent;
        }

        Activity activity = getActivity();
        if (activity instanceof DaggerActivity) {
            fragmentComponent = ((DaggerActivity) activity).getComponent().plus(new FragmentModule(this));
            return fragmentComponent;
        } else {
            throw new IllegalStateException("Fragment has to be within DaggerActivity.");
        }
    }
}
