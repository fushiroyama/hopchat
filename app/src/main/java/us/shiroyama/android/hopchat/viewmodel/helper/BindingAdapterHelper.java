package us.shiroyama.android.hopchat.viewmodel.helper;

import android.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.text.TextUtils;
import android.text.util.Linkify;
import android.util.Patterns;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.regex.Pattern;

import us.shiroyama.android.hopchat.R;

/**
 * Helper using {@link BindingAdapter}
 *
 * @author Fumihiko Shiroyama (fu.shiroyama@gmail.com)
 */

public class BindingAdapterHelper {
    @BindingAdapter("iconImageUrl")
    public static void setIconImageUrl(@NonNull ImageView imageView, @Nullable String imageUrl) {
        Drawable drawable = ContextCompat.getDrawable(imageView.getContext(), R.drawable.ic_web_black_24dp);
        DrawableCompat.setTint(drawable, ContextCompat.getColor(imageView.getContext(), R.color.placeholder));
        if (TextUtils.isEmpty(imageUrl)) {
            imageView.setImageDrawable(drawable);
            return;
        }

        Glide.with(imageView.getContext())
                .load(Uri.parse(imageUrl))
                .placeholder(drawable)
                .error(drawable)
                .into(imageView);
    }

    @BindingAdapter("linkify")
    public static void setLinkify(@NonNull TextView textView, @NonNull String text) {
        textView.setText(text);

        Linkify.TransformFilter filter = (match, url) -> match.group();

        Pattern mentionPattern = Pattern.compile("\\([a-zA-Z0-9]+\\)");
        Pattern emoticonPattern = Pattern.compile("@[a-zA-Z0-9]+");
        Pattern webPattern = Patterns.WEB_URL;

        Linkify.addLinks(textView, mentionPattern, null, null, filter);
        Linkify.addLinks(textView, emoticonPattern, null, null, filter);
        Linkify.addLinks(textView, webPattern, null, null, filter);
    }
}
