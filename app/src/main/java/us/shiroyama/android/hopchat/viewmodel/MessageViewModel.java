package us.shiroyama.android.hopchat.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.view.View;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import us.shiroyama.android.hopchat.entity.Message;

/**
 * ViewModel for Message
 *
 * @author Fumihiko Shiroyama (fu.shiroyama@gmail.com)
 */

public class MessageViewModel extends BaseObservable {
    private final Message message;

    private final OkHttpClient okHttpClient;

    public final ObservableField<String> messageText;

    public final ObservableInt additionalSectionVisibility = new ObservableInt(View.GONE);

    public final ObservableArrayList<LinkViewModel> linkViewModels = new ObservableArrayList<>();

    @Inject
    public MessageViewModel(Message message, OkHttpClient okHttpClient) {
        this.message = message;
        this.okHttpClient = okHttpClient;

        this.messageText = new ObservableField<>(message.getMessage());

        linkViewModels.addAll(
                Stream.of(message.getLinks())
                        .map(link -> new LinkViewModel(link, okHttpClient)).collect(Collectors.toList())
        );

        additionalSectionVisibility.set(linkViewModels.size() > 0 ? View.VISIBLE : View.GONE);
    }
}
