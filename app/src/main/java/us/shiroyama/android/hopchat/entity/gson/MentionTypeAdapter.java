package us.shiroyama.android.hopchat.entity.gson;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

import us.shiroyama.android.hopchat.entity.Message.Mention;

/**
 * TypeAdapter for {@link Mention}
 *
 * @author Fumihiko Shiroyama (fu.shiroyama@gmail.com)
 */

public class MentionTypeAdapter implements JsonSerializer<Mention>, JsonDeserializer<Mention> {
    public static MentionTypeAdapter newInstance() {
        return new MentionTypeAdapter();
    }

    @Override
    public Mention deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        return new Mention(json.getAsString());
    }

    @Override
    public JsonElement serialize(Mention src, Type typeOfSrc, JsonSerializationContext context) {
        return new JsonPrimitive(src.getName());
    }
}
