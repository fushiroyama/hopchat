package us.shiroyama.android.hopchat.di.components;

import javax.inject.Singleton;

import dagger.Component;
import us.shiroyama.android.hopchat.MyApplication;
import us.shiroyama.android.hopchat.di.modules.ActivityModule;
import us.shiroyama.android.hopchat.di.modules.ApplicationModule;

/**
 * Application {@link Component}
 *
 * @author Fumihiko Shiroyama (fu.shiroyama@gmail.com)
 */

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {
    void inject(MyApplication application);

    ActivityComponent plus(ActivityModule module);
}
