package us.shiroyama.android.hopchat.viewmodel;

import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.support.annotation.NonNull;
import android.view.View;

import com.annimon.stream.Optional;
import com.annimon.stream.Stream;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

import javax.inject.Inject;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import timber.log.Timber;
import us.shiroyama.android.hopchat.entity.Message;

/**
 * ViewModel for Link
 *
 * @author Fumihiko Shiroyama (fu.shiroyama@gmail.com)
 */

public class LinkViewModel {
    public final ObservableField<String> title = new ObservableField<>();
    public final ObservableField<String> description = new ObservableField<>();
    public final ObservableField<String> iconURL = new ObservableField<>();
    public final ObservableInt visibility = new ObservableInt(View.GONE);

    @Inject
    public LinkViewModel(@NonNull Message.Link link, @NonNull OkHttpClient okHttpClient) {
        HttpUrl httpUrl = HttpUrl.parse(link.getUrl());
        Request request = new Request.Builder()
                .url(httpUrl)
                .get()
                .build();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Timber.d(e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                ResponseBody responseBody = response.body();
                Document document = Jsoup.parse(responseBody.string());

                Optional.of(document.title()).ifPresent(titleText -> {
                    Timber.d("titleText: %s", titleText);
                    title.set(titleText);
                });

                Stream.of(document.head().getElementsByTag("meta").select("[name=description]"))
                        .findFirst()
                        .ifPresent(element -> {
                            String descriptionText = element.attr("content");
                            Timber.d("descriptionText: %s", descriptionText);
                            description.set(descriptionText);
                        });

                Stream.of(document.head().getElementsByTag("link"))
                        .findFirst()
                        .ifPresent(element -> {
                            String href = element.attr("href");
                            Timber.d("href: %s", href);

                            String iconUrl = "";
                            if (href != null && href.startsWith("/")) {
                                iconUrl = httpUrl.scheme() + "://" + httpUrl.host() + href;
                            } else if (href != null && href.startsWith("http")) {
                                iconUrl = httpUrl.url().toString();
                            }
                            Timber.d("iconUrl: %s", iconUrl);
                            iconURL.set(iconUrl);
                        });

                visibility.set(View.VISIBLE);
            }
        });
    }
}
