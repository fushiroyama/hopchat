package us.shiroyama.android.hopchat.util;

import android.support.annotation.NonNull;

/**
 * Message Utilities
 *
 * @author Fumihiko Shiroyama (fu.shiroyama@gmail.com)
 */

public class MessageUtils {
    private static final int EMOTICON_MAX_LENGTH = 15;

    public static boolean isAlphaNumeric(char c) {
        return Character.isLetterOrDigit(c);
    }

    public static boolean isAlphaNumeric(@NonNull String text) {
        return text.matches("[a-zA-Z0-9]+");
    }

    public static boolean isValidEmoticon(@NonNull String emoticon) {
        return emoticon.length() <= EMOTICON_MAX_LENGTH && isAlphaNumeric(emoticon);
    }

    public static boolean isSeparator(char c) {
        return c == ' ' || c == '\r' || c == '\n' || c == '\t';
    }

}
