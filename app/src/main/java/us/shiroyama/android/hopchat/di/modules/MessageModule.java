package us.shiroyama.android.hopchat.di.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import us.shiroyama.android.hopchat.util.parser.MessageParser;

/**
 * Message {@link Module}
 *
 * @author Fumihiko Shiroyama (fu.shiroyama@gmail.com)
 */

@Module
public class MessageModule {

    @Singleton
    @Provides
    public MessageParser providesMessageParser() {
        return new MessageParser();
    }
}
