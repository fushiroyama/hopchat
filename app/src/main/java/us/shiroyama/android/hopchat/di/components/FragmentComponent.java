package us.shiroyama.android.hopchat.di.components;

import dagger.Subcomponent;
import us.shiroyama.android.hopchat.di.modules.FragmentModule;
import us.shiroyama.android.hopchat.di.scopes.FragmentScope;
import us.shiroyama.android.hopchat.view.fragment.ChatFragment;

/**
 * Fragment {@link Subcomponent}
 *
 * @author Fumihiko Shiroyama (fu.shiroyama@gmail.com)
 */

@FragmentScope
@Subcomponent(modules = FragmentModule.class)
public interface FragmentComponent {
    void inject(ChatFragment fragment);
}
