package us.shiroyama.android.hopchat.view.activity;

import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import us.shiroyama.android.hopchat.MyApplication;
import us.shiroyama.android.hopchat.di.components.ActivityComponent;
import us.shiroyama.android.hopchat.di.modules.ActivityModule;

/**
 * Dagger capable {@link AppCompatActivity}
 *
 * @author Fumihiko Shiroyama (fu.shiroyama@gmail.com)
 */

public abstract class DaggerActivity extends AppCompatActivity {
    private ActivityComponent activityComponent;

    @NonNull
    public ActivityComponent getComponent() {
        if (activityComponent == null) {
            MyApplication application = (MyApplication) getApplication();
            activityComponent = application.getComponent().plus(new ActivityModule(this));
        }
        return activityComponent;
    }

    final void replaceFragment(@NonNull Fragment fragment, @IdRes @LayoutRes int layoutResId) {
        final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(layoutResId, fragment, fragment.getClass().getSimpleName());
        ft.commit();
    }
}
