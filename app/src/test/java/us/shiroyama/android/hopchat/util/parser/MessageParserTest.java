package us.shiroyama.android.hopchat.util.parser;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import us.shiroyama.android.hopchat.entity.Message;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test for {@link MessageParser}
 *
 * @author Fumihiko Shiroyama (fu.shiroyama@gmail.com)
 */

public class MessageParserTest {
    private MessageParser messageParser;

    @Before
    public void setUp() throws Exception {
        messageParser = new MessageParser();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void parse() throws Exception {
        String source = "@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016";
        Message message = messageParser.parse(source);
        assertThat(message).isNotNull();

        List<Message.Mention> mentions = message.getMentions();
        List<String> names = Stream.of(mentions).map(Message.Mention::getName).collect(Collectors.toList());
        assertThat(names).containsOnly("bob", "john");

        List<Message.Emoticon> emoticons = message.getEmoticons();
        List<String> labels = Stream.of(emoticons).map(Message.Emoticon::getLabel).collect(Collectors.toList());
        assertThat(labels).containsOnly("success");

        List<Message.Link> links = message.getLinks();
        List<String> urls = Stream.of(links).map(Message.Link::getUrl).collect(Collectors.toList());
        assertThat(urls).containsOnly("https://twitter.com/jdorfman/status/430511497475670016");
    }

}