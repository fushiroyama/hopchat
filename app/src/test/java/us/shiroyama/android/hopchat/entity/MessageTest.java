package us.shiroyama.android.hopchat.entity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import us.shiroyama.android.hopchat.entity.gson.EmoticonTypeAdapter;
import us.shiroyama.android.hopchat.entity.gson.MentionTypeAdapter;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test for {@link Message}
 *
 * @author Fumihiko Shiroyama (fu.shiroyama@gmail.com)
 */

public class MessageTest {
    private final Gson gson = new GsonBuilder()
            .excludeFieldsWithoutExposeAnnotation()
            .registerTypeAdapter(Message.Mention.class, new MentionTypeAdapter())
            .registerTypeAdapter(Message.Emoticon.class, new EmoticonTypeAdapter())
            .create();

    private Message message;

    @Before
    public void setUp() throws Exception {
        List<Message.Mention> mentions = new ArrayList<Message.Mention>() {
            {
                Message.Mention bob = new Message.Mention();
                bob.setName("bob");
                Message.Mention john = new Message.Mention();
                john.setName("john");
                add(bob);
                add(john);
            }
        };
        List<Message.Emoticon> emoticons = new ArrayList<Message.Emoticon>() {
            {
                Message.Emoticon megusta = new Message.Emoticon("megusta");
                Message.Emoticon coffee = new Message.Emoticon("coffee");
                add(megusta);
                add(coffee);
            }
        };
        List<Message.Link> links = new ArrayList<Message.Link>() {
            {
                Message.Link linkTwitter = new Message.Link();
                linkTwitter.setUrl("https://twitter.com/jdorfman/status/430511497475670016");
                linkTwitter.setTitle("Twitter / jdorfman: nice @littlebigdetail from ...");
                add(linkTwitter);
            }
        };
        message = new Message();
        message.setMentions(mentions);
        message.setEmoticons(emoticons);
        message.setLinks(links);
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void encode() throws Exception {
        String expected = "{\"mentions\":[\"bob\",\"john\"],\"emoticons\":[\"megusta\",\"coffee\"],\"links\":[{\"url\":\"https://twitter.com/jdorfman/status/430511497475670016\",\"title\":\"Twitter / jdorfman: nice @littlebigdetail from ...\"}]}";
        String actual = gson.toJson(message);
        assertThat(actual)
                .isNotEmpty()
                .contains(expected);
    }

}