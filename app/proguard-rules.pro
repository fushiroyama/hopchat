# ----------------------------------------
# Support Library
# ----------------------------------------
-dontwarn android.support.**
-keep class android.support.** { *; }

# ----------------------------------------
# retrolambda
# ----------------------------------------
-dontwarn java.lang.invoke.*

# ----------------------------------------
# OkHttp
# ----------------------------------------
-dontwarn okio.**

# ----------------------------------------
# Glide
# ----------------------------------------
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}
-keepresourcexmlelements manifest/application/meta-data@value=GlideModule